package lambda;

public class GCD {

	public static void main(String[] args){
		System.out.println("-------------------");
		GCDInterface getGCD = (x,y) -> gcd(x,y);
		int result = getGCD.getgcd(43, 3);
		System.out.println(result);
		
	}
	public static int gcd(int x,int y){
		return y==0?x: gcd(y,x%y);
	}
}