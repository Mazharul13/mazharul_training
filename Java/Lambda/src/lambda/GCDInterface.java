/**
 * 
 */
package lambda;

/**
 * @author mazharul.islam
 *
 */
@FunctionalInterface
public interface GCDInterface {
	 int getgcd(int x, int y);	 
}
