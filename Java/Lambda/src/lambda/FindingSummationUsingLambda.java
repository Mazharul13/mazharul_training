package lambda;
import java.util.ArrayList;


public class FindingSummationUsingLambda {

	public static void main(String[] args) {
		ArrayList<Integer> numberArray = new ArrayList<Integer>();
		for(int i=0;i<10;i++){
			numberArray.add(i);
		}
		Integer summation =  numberArray.stream().mapToInt(i -> i).sum() ;
		System.out.println(summation);
	}
}
