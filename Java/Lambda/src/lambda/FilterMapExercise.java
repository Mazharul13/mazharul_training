package lambda;
import java.util.ArrayList;
import java.util.function.Predicate;


public class FilterMapExercise {
	public static void main(String [] args){
		// creating an array of persons using random values
		ArrayList<Engineer> engineerList = new ArrayList<Engineer>();
		
		engineerList.add(new Engineer(34,"CSE","Mazharul"));
		// Extract Data		
		Predicate<Engineer> predicate = p -> p.getAge()<35 && p.getSubject() == "CSE";
		ExtractData extractData = new ExtractData();
		extractData.filterData(engineerList,predicate);
	}
}

class Engineer{
	private int age;
	private String subject;
	private String name;
	
	Engineer(int age,String subject,String name){
		this.age = age;
		this.subject = subject;
		this.name = name;
	}
	int getAge() {
		return this.age;
	}
	String getSubject(){
		return this.subject;
	}
	void show(){
		System.out.println("Age : "+this.age) ;
		System.out.println("Subject : "+this.subject) ;
		System.out.println("Name : "+this.name) ;
	}
}

class ExtractData{
	public void filterData(ArrayList<Engineer> engineerList,Predicate<Engineer> pred){
		for(Engineer eng:engineerList){
			if(pred.test(eng)){
				eng.show();
			}
		}
	}
	ExtractData(){
		
	}
}