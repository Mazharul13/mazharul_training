package com.qi.app;

/**
 * Hello world!
 *
 */

import com.google.common.base.Converter;

public class App extends Converter<Back, Front>{

	private long keyValue = 0 ;
	@Override
	protected Back doBackward(Front front) {
		Back back = new Back();
		back.setAge(front.getAge());
		back.setName(front.getName());
		back.setPrimaryKey(keyValue++);
		return back ;
	}

	@Override
	protected Front doForward(Back back) {
		Front front = new Front();
		front.setAge(back.getAge());
		front.setName(back.getName());
	
		return front;
	}	
}

class Front{
	private String name;
	private int age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
class Back{

	private long primaryKey;
	private String name;
	private int age;
	
	
	public long getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(long primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
}
