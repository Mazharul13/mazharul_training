package org.gradle;

import com.google.inject.AbstractModule;
public class ModuleTest extends AbstractModule {

	@Override
	protected void configure() {
		// blind the implementations to the classes
		bind(MyInterface.class).to(ImplementationTest.class)	;
	}
	
}
