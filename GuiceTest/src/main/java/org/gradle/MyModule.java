/**
 * 
 */
package org.gradle;

import com.google.inject.AbstractModule;
import com.google.inject.Module;

/**
 * @author mazharul.islam
 *
 */
public class MyModule extends AbstractModule implements Module {
	@Override
	protected void configure() {
			bind(MyInterface.class).to(Implementation1.class);
	}

}
