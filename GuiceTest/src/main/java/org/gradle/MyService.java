package org.gradle;

import com.google.inject.Inject;

public class MyService {
	
	private MyInterface myInterface = null;
	
	@Inject
	public MyService(MyInterface myInterface){
		this.myInterface = myInterface;
	}
	public void getService(){
		myInterface.getInterfaceImplementation();
	}

}
