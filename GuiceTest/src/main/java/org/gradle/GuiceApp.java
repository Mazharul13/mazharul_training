package org.gradle;
import com.google.inject.Guice;
import com.google.inject.Injector;
public class GuiceApp {

	public static void main(String[] args) {
		Injector guice = Guice.createInjector(new MyModule());
		Injector guiceTest = Guice.createInjector(new ModuleTest());
		MyService service = guice.getInstance(MyService.class);
		service = guiceTest.getInstance(MyService.class);
		
		service.getService();
	}

}
